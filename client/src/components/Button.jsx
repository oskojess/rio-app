import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledButton = styled.button`
  ${props => `min-width: ${props.minWidth || '120px'};`}
  ${props => props.margin && props.margin.top && `margin-top: ${props.margin.top};`}
  ${props => props.margin && props.margin.bottom && `margin-bottom: ${props.margin.bottom};`}
  ${props => props.margin && props.margin.left && `margin-left: ${props.margin.left};`}
  ${props => props.margin && props.margin.right && `margin-right: ${props.margin.right};`}
`

const Button = ({ type, onClick, children, disabled, styles }) => (
  <StyledButton
    className={`button ${styles}`}
    disabled={disabled}
    type={type}
    onClick={onClick}
  >
    {children}
  </StyledButton>
)

export default Button

const { node, bool, string, shape, oneOfType, number } = PropTypes

Button.propTypes = {
  /** Children nodes. */
  children: node,
  /** Disable button. */
  disabled: bool,
  /** Button type. */
  type: string,
  /** Button min-width. */
  minWidth: string,
  /** Button margin. */
  margin: shape({
    top: oneOfType([string, number]),
    bottom: oneOfType([string, number]),
    left: oneOfType([string, number]),
    right: oneOfType([string, number])
  }),
}

Button.defaultProps = {
  type: 'button',
  disabled: false,
  style: 'is-rounded normal is-danger',
  margin: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  }
}