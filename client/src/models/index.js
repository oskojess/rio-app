import example from './example'
import authModel from './auth'
import registerModel from './register'

export default { example, authModel, registerModel }